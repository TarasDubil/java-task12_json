package com.dubilok.parser.jackson;

import com.dubilok.model.GemStone;
import com.dubilok.model.Gems;
import com.dubilok.model.GemsStoneComparator;
import com.dubilok.validator.Validator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class JacksonParser {

    private static Logger logger = LogManager.getLogger(JacksonParser.class);

    public JacksonParser() {
        run();
    }

    public static List<GemStone> parseToJava() {
        try {
            if (Validator.validate("schema.json", "gems.json")) {
                Gems gems = read("gems.json");
                List<GemStone> gemStones = Arrays.asList(gems.getGemstone());
                return gemStones;
            }
        } catch (IOException e) {
            logger.error(e);
        }
        return null;
    }

    public static Gems read(String path) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(new File(path), Gems.class);
    }

    public void run() {
        List<GemStone> gemStoneList = parseToJava();
        gemStoneList.sort(new GemsStoneComparator());
        gemStoneList.forEach(k -> logger.info(k));
        try {
            parseToJson(gemStoneList);
        } catch (IOException e) {
            logger.error(e);
        }
    }

    public static void parseToJson(List<GemStone> gemStone) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(new BufferedWriter(new FileWriter("newJackson.json")), gemStone);
    }
}
