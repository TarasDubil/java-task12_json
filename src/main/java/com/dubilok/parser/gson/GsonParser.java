package com.dubilok.parser.gson;

import com.dubilok.model.GemStone;
import com.dubilok.model.Gems;
import com.dubilok.model.GemsStoneComparator;
import com.dubilok.validator.Validator;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class GsonParser {

    private static Logger logger = LogManager.getLogger(GsonParser.class);

    public List<GemStone> parseToJava() throws FileNotFoundException {
        try {
            if (Validator.validate("schema.json", "gems.json")) {
                Gems gems = read("gems.json");
                List<GemStone> gemStoneList = Arrays.asList(gems.getGemstone());
                return gemStoneList;
            }
        } catch (IOException e) {
            logger.error(e);
        }
        return null;
    }

    private Gems read(String path) throws FileNotFoundException {
        Gson gson = new Gson();
        return gson.fromJson(new FileReader(path), Gems.class);
    }

    public void run() throws IOException {
        List<GemStone> gemStones = parseToJava();
        gemStones.sort(new GemsStoneComparator());
        gemStones.forEach(k -> logger.info(k));
        parseToJson(gemStones);
    }

    private void writeDataToJson(String data) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter("newJsonGson.json"));
        writer.write(data);
        writer.flush();
        writer.close();
    }

    public void parseToJson(List<GemStone> gemStone) throws IOException {
        Gson gson = new Gson();
        String toJson = gson.toJson(gemStone);
        writeDataToJson(toJson);
    }
}
