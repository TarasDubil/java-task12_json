package com.dubilok.controller;

import com.dubilok.parser.gson.GsonParser;
import com.dubilok.parser.jackson.JacksonParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class ControllerImpl implements Controller {

    private static Logger logger = LogManager.getLogger(ControllerImpl.class);

    @Override
    public void showGsonParser() {
        try {
            new GsonParser().run();
        } catch (IOException e) {
            logger.error(e);
        }
    }

    @Override
    public void showJacksonParser() {
        new JacksonParser();
    }
}
