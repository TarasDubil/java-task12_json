package com.dubilok.controller;

public interface Controller {
    void showGsonParser();

    void showJacksonParser();
}
