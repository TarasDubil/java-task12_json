package com.dubilok.model;

import java.util.Comparator;

public class GemsStoneComparator implements Comparator<GemStone> {
    @Override
    public int compare(GemStone o1, GemStone o2) {
        return Double.compare(o1.getValue(), o2.getValue());
    }
}
