package com.dubilok.model;

public class GemStone {
    private int gemstoneNo;
    private String name;
    private boolean preciousness;
    private String origin;
    private double value;
    private VisualParameters visualParameters;

    public GemStone() {
    }

    public GemStone(String name, boolean preciousness, String origin, int value, VisualParameters visualParameters) {
        this.name = name;
        this.preciousness = preciousness;
        this.origin = origin;
        this.value = value;
        this.visualParameters = visualParameters;
    }

    public int getGemstoneNo() {
        return gemstoneNo;
    }

    public void setGemstoneNo(int gemstoneNo) {
        this.gemstoneNo = gemstoneNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPreciousness() {
        return preciousness;
    }

    public void setPreciousness(boolean preciousness) {
        this.preciousness = preciousness;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    @Override
    public String toString() {
        return "GemStone{" +
                "name='" + name + '\'' +
                ", preciousness=" + preciousness +
                ", origin='" + origin + '\'' +
                ", value=" + value +
                ", visualParameters=" + visualParameters +
                '}';
    }
}
