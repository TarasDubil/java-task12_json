package com.dubilok.model;

public class Gems {
    private GemStone[] gemstone;

    public Gems() {
    }

    public GemStone[] getGemstone() {
        return gemstone;
    }

    public void setGemstone(GemStone[] gemstone) {
        this.gemstone = gemstone;
    }
}
